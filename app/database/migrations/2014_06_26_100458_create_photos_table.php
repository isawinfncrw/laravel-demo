<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('photos', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('width')->default(0)->unsigned();
            $table->integer('height')->default(0)->unsigned();
            $table->string('url', 255)->default('');
            $table->integer('sort')->default(0)->unsigned();
            $table->smallInteger('position')->default(0)->unsigned();
            $table->smallInteger('target')->default(1)->unsigned();
            $table->string('img_ver', 255)->default('');
            $table->smallInteger('state')->default(2)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('photos');
    }

}
