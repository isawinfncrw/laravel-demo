<?php

return array(
    'comment_photo' => 'comment to photo',
    'add_photo' => 'add a photo',
    'remove_comment' => 'remove a comment',
    'remove_photo' => 'remove a photo',
    'list_photo' => 'photo list',
);
