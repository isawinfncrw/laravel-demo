<?php

namespace event\event_20140630_photo_upload;

class PhotoController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    public function attachPhoto() {
        $ym = date('Ym');
        $ymdhis = date('YmdHis');
        $rnd = str_random(8);
        $dest_path = "uploads/article-image/{$ym}";
        $sn = "{$ymdhis}{$rnd}";
        if (\Input::file('main-image')) {
            $position = 1;
            $upload_success = \Input::file('main-image')->move($dest_path, "{$sn}.jpg");

            if ($upload_success) {
                $tn = \Intervention\Image\Image::make(public_path() . "/{$dest_path}/{$sn}.jpg")->resize(320, 240, true, false);
                $tn->save(public_path() . "/{$dest_path}/{$sn}-tn.jpg");
                $img = new Photo();
                $img->position = 1;
                $img->img_ver = "{$dest_path}/{$sn}";
                $img->save();
            }
        }
    }

    public function addPhoto() {
        $this->attachPhoto();
        return \Redirect::to("event/20140630-photo-upload/list-photo");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function addPhotoForm() {
        $photo = new Photo();
        $data = array();
        return \View::make('event.event_20140630_photo_upload.photo.add', array('photo' => $photo, 'data' => $data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    public function listPhoto() {
        $photo_list = Photo::paginate(1);
        $data = array();
        $data['photo_list'] = $photo_list;
        return \View::make('event.event_20140630_photo_upload.photo.list', array('data' => $data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function remove($id) {
        $photo = Photo::findOrFail($id);
        $img_ver = $photo->img_ver;


        \File::delete(public_path("{$img_ver}.jpg"));
        \File::delete(public_path("{$img_ver}-tn.jpg"));


        $photo->comments()->delete();
        $photo->delete();
        return \Redirect::to("event/20140630-photo-upload/list-photo");
    }

}
